#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import argparse

class Main(object):

    def operations_in_brackets(self, listwithbrackets):   #функция исполнения выражения в скобках
        Error = False

        for element in listwithbrackets:    #проход по элементам данного списка

            if element == '(':  #если элемент равен (

                #индексы начала и конца скобок
                try:
                    indexfrom = int(listwithbrackets.index('('))
                    indexto = int(listwithbrackets.index(')'))
                except ValueError:
                    return 'Закройте скобки'
                    break
                #print(listwithbrackets, ' ', indexfrom, ' : ', indexto) #Включается для отладки

                #Блок исполнения выражений 
                i = indexfrom
                while i < len(listwithbrackets[indexfrom:indexto]) + indexfrom: #цикл для */

                    if listwithbrackets[i] == '*':
                        res = float(listwithbrackets[i-1])*float(listwithbrackets[i+1]) # в результат забиваем выражение
                        listwithbrackets.pop(i-1)
                        listwithbrackets.pop(i-1)   #Удаляем лишние элементы
                        listwithbrackets.pop(i-1)
                        listwithbrackets.insert(i-1,res)    #Заносим результат в начальный индекс
                        indexto -= 2    #уменьшаем значение индекса конца скобок
                        i -= 2
                        #print(listwithbrackets, ' ', indexfrom, ' : ', indexto) #Включается для отладки

                    if listwithbrackets[i] == '/':
                        try:
                            res = float(listwithbrackets[i-1])/float(listwithbrackets[i+1])
                            listwithbrackets.pop(i-1)
                            listwithbrackets.pop(i-1)
                            listwithbrackets.pop(i-1)
                            listwithbrackets.insert(i-1,res)
                            indexto -= 2
                            i -= 2
                            #print(listwithbrackets, ' ', indexfrom, ' : ', indexto) #Включается для отладки
                        except ZeroDivisionError:
                            Error = True
                            return 'Делить на ноль нельзя!'
                            break
                    i += 1

                if Error:
                    break

                i = indexfrom
                while i < len(listwithbrackets[indexfrom:indexto]) + indexfrom: #цикл для +-

                    if listwithbrackets[i] == '+':
                        res = float(listwithbrackets[i-1])+float(listwithbrackets[i+1])
                        listwithbrackets.pop(i-1)
                        listwithbrackets.pop(i-1)
                        listwithbrackets.pop(i-1)
                        listwithbrackets.insert(i-1,res)
                        indexto -= 2
                        i -= 2
                        #print(listwithbrackets, ' ', indexfrom, ' : ', indexto) #Включается для отладки

                    if listwithbrackets[i] == '-':
                        res = float(listwithbrackets[i-1])-float(listwithbrackets[i+1])
                        listwithbrackets.pop(i-1)
                        listwithbrackets.pop(i-1)
                        listwithbrackets.pop(i-1)
                        listwithbrackets.insert(i-1,res)
                        indexto -= 2
                        i -= 2
                        #print(listwithbrackets, ' ', indexfrom, ' : ', indexto) #Включается для отладки
                    i += 1

                listwithbrackets[indexfrom] = listwithbrackets[indexfrom + 1]   #забиваем в начало скобок результат и удаляем последующие ненужные элементы
                listwithbrackets.pop(indexfrom+1)
                listwithbrackets.pop(indexfrom+1)
        return Error

    def mainCount(self, listinput):
        Error = False

        i = 0
        while i < len(listinput): #цикл для */

            if listinput[i] == '*':
                res = float(listinput[i-1])*float(listinput[i+1]) # в результат забиваем выражение
                listinput.pop(i-1)
                listinput.pop(i-1)   #Удаляем лишние элементы
                listinput.pop(i-1)
                listinput.insert(i-1,res)    #Заносим результат в начальный индекс
                i -= 2
                #print(listinput) #Включается для отладки

            if listinput[i] == '/':
                try:
                    res = float(listinput[i-1])/float(listinput[i+1])
                    listinput.pop(i-1)
                    listinput.pop(i-1)
                    listinput.pop(i-1)
                    listinput.insert(i-1,res)
                    i -= 2
                    #print(listinput) #Включается для отладки
                except ZeroDivisionError:
                    Error = True
                    return 'Делить на ноль нельзя!'
                    break
            i += 1

        if not Error:
            i = 0
            while i < len(listinput): #цикл для +-

                if listinput[i] == '+':
                    res = float(listinput[i-1])+float(listinput[i+1])
                    listinput.pop(i-1)
                    listinput.pop(i-1)
                    listinput.pop(i-1)
                    listinput.insert(i-1,res)
                    i -= 2
                    #print(listinput) #Включается для отладки

                if listinput[i] == '-':
                    res = float(listinput[i-1])-float(listinput[i+1])
                    listinput.pop(i-1)
                    listinput.pop(i-1)
                    listinput.pop(i-1)
                    listinput.insert(i-1,res)
                    i -= 2
                    #print(listinput) #Включается для отладки
                i += 1
        return Error

    def doCount(self, string):
        Error = False
        listcalc =[]
        chislo = ''

        Canpass = False

        for element in string:

            if element in '+-/*':
                if not Canpass:
                    if chislo == '':
                        chislo = chislo + element
                    else:
                        Error = True
                if not chislo == '': 
                    listcalc.append(chislo)
                    chislo = ''
                if Canpass:
                    listcalc.append(element)
                    Canpass = False
            elif element in '()':
                if not chislo == '': 
                    listcalc.append(chislo)
                    chislo = ''
                Canpass = True
                listcalc.append(element)
            elif element in '1234567890,.':
                chislo = chislo + element
                Canpass = True
            elif element == ' ':
                None
            else:
                Error = True
                return 'Ошибка ввода!'
        if not chislo == '': 
            listcalc.append(chislo)
            chislo = ''

        #print(listcalc, ' ', len(listcalc)) #Включается для отладки

        try:
            if Error == False:
                Error = self.operations_in_brackets(listcalc)
                if Error == False:
                    Error = self.mainCount(listcalc)
                if Error == False:
                    result = float(listcalc[0])
                    return result
                else:
                    return Error
        except ValueError:
            return 'Ошибка ввода!'
        except IndexError:
            return 'NNN'



