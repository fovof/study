#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
from PyQt5.QtWidgets import (QApplication, QWidget, QGridLayout, QLineEdit, QLabel, QMessageBox, QToolTip,
    QPushButton)
import os
import core
import datetime

#класс интерфейса калькулятора
class CalculatorInterface(QWidget):

    #Функция запуск
    def __init__(self):
        super().__init__()

        self.initUI()    #Запуск интерфейса
        self.expCalculator = core.Main()    #Импортируем ядро калькулятора

    #Функция инициирования интерфейса пользователя
    def initUI(self):
        
        grid = QGridLayout()    #Создаем сеточный макет
        self.setLayout(grid)    #Устанавливаем сеточный макет в качестве разметки

        self.input = QLineEdit(readOnly=True)    #Задаем строку ввода выражения (открыта только для чтения)
        self.output = QLineEdit(readOnly=True, text="Ответ")     #Задаем строку вывода ответа (открыта только для чтения)
        
        #Задаем имена будущих кнопок
        names = ['', '', '', '', '',
                'Cls', 'Bck', '=', '.', 'Close',
                 '7', '8', '9', '/', 'Clr Log',
                 '4', '5', '6', '*', '',
                 '1', '2', '3', '-', '',
                 '(', '0', ')', '+', '',]

        positions = [(i,j) for i in range(6) for j in range(5)] #Двумерный массив с позициями макета

        grid.addWidget(self.input, 0, 0, 1, 3)    #Добавляем на макет поля ввода и вывода
        grid.addWidget(self.output, 0, 3, 1, 2)
        
        #Цикл для создания и расстановки кнопок
        for position, name in zip(positions, names):

            if name == '':    #пропуск пустых имен
                continue

            button = QPushButton(name)    #Создаем кнопку
            button.clicked.connect(self.buttonClicked)    #Действие по нажатию на кнопку
            grid.addWidget(button, *position)    #Привязываем кнопку к позиции на макете

        self.input.textChanged.connect(self.Counter)    #Действие по изменению состояния строки ввода
        self.move(300, 150)    #Позиция на экране
        self.setWindowTitle('Calculator')    #Заголовок
        self.setFixedSize(500,200)    #Фиксированный размер окна
        self.show()
    
    #Функция клика по кнопке
    def buttonClicked(self):
        sender = self.sender()    #Получаем название кнопки
        name = sender.text()

        if name in '1234567890()-+/*.':    #Условие на кнопки со знаками
            self.buttonCount(name)
        
        if name == '=':
            self.Counter()

        if name == 'Cls':
            self.saveLog()
            self.input.clear()
            self.output.clear()

        if name == 'Close':
            self.saveLog()
            sys.exit()

        if name == 'Bck':
            self.backspace()

        if name == 'Clr Log':
            os.system('rm logs.log')

    #Функция удаления последнего символа
    def backspace(self):
        prtext=self.input.text()    #Берем текст из ввода
        nxttext=prtext[0:len(prtext)-1]    #Убираем последний символ
        self.input.setText(nxttext)    #Записываем результат в строку ввода

    #Функция добавления символа
    def buttonCount(self, name):
        prtext=self.input.text()
        nxttext=prtext+name
        self.input.setText(nxttext)

    #Функция счетчика выражения
    def Counter(self):
        text = self.input.text()
        text.replace('(','\\(')    #Экранируем скобки
        result = str(self.expCalculator.doCount(text))    #Считаем выражение через импортированную функцию
        if result != 'NNN':    #Проверка на IndexError (для правильности отображения)
            self.output.setText(result)
    
    #Функция логирования
    def saveLog(self):
        if (self.output.text() != '') & (self.output.text() != 'Ответ'):
            date = str(datetime.datetime.today())
            try:
                os.system('touch logs.log')
            except FileExistsError:
                None

            file = open('logs.log', 'a')
            file.write('[' + date + '] || ' + self.input.text() + ' = ' + self.output.text() + '\n')
            file.close()

if __name__ == '__main__':

    #Запуск программы
    app = QApplication(sys.argv)
    ex = CalculatorInterface()
    sys.exit(app.exec_())