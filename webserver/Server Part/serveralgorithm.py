import os
import json
import urllib.request
import subprocess
import pandas
from pyparsing import *

def input(data):
	try:
		identificator = json.loads(data,object_hook=ident)
		accesspassword = json.loads(data, object_hook=password)
		idbase = pandas.read_csv("idbase")
		#print(idbase)
		#print(idbase.get_value(idbase[idbase.id == identificator].index[0],"password"), accesspassword)
		if str(accesspassword) != str(idbase.get_value(idbase[idbase.id == identificator].index[0],"password")):
			return identificator, "Undefined", "Password error"
		rights = idbase.get_value(idbase[idbase.id == identificator].index[0],"rights")

		if json.loads(data,object_hook=command) == "create":
			algname, result = create(data, rights)
			return identificator, algname, result

		elif json.loads(data,object_hook=command) == "show":
			algname, result = show(data, rights)
			return identificator, algname, result

		elif json.loads(data,object_hook=command) == "delete":
			algname, result = delete(data, rights)
			return identificator, algname, result

		elif json.loads(data,object_hook=command) == "use":
			algname, result = use(data, rights)
			return identificator, algname, result

		else:
			return identificator, "Undefined", "Bad command"

	except Exception:
		return "Undefined", "Undefined", "Read error"

def create(data, rights):
	try:
		algname = json.loads(data,object_hook=name)
		alg = json.loads(data,object_hook=algorithm)
		arguments = alg.count("input()")
		if (alg.count("sys.stdout.write") != 1):
			return algname, "Bad code"

		try:
			os.mkdir('./algorithms')
			os.touch('./algorithms/rights')
		except FileExistsError:
			None

		rightsbase = pandas.read_csv("algorithms/rights", index_col=0)

		if len(rightsbase[rightsbase.name == algname]) != 0:
			return algname, "Algorithm exists"

		newcont = pandas.DataFrame({'name':[algname], 'rights':[rights], 'arguments':[arguments]}, columns=['name','rights','arguments'])
		rightsbase = rightsbase.append(newcont, ignore_index=True)
		rightsbase.to_csv("algorithms/rights")

		algfile = open('./algorithms/' + algname, 'w')
		algfile.write(alg)
		algfile.close()

	except Exception:
		return "Undefined", "Creation is unsuccessful"
	else:
		return algname, "Creation is successful"

def use(data, rights):
	try:
		algname = json.loads(data,object_hook=name)
		arguments = json.loads(data,object_hook=args)
		argsmass = []
		argumentmask = OneOrMore(Word(alphanums)^quotedString)
		argsmass = argumentmask.parseString(arguments)
		for elem in argsmass:
			try:
				elem.replace('\"','')
			except Exception:
				None
		rightsbase = pandas.read_csv("algorithms/rights", index_col=0)
		if (rights != rightsbase.get_value(rightsbase[rightsbase.name == algname].index[0],"rights")) and (rights != "admin"):
			return algname, "Do not enough rights to use"

		process = subprocess.Popen(['python3', './algorithms/'+algname], stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, universal_newlines=True)
		while len(argsmass)>0:
			process.stdin.write(argsmass.pop() + "\n")
			process.stdin.flush()

		result = process.stdout.readline()
		process.terminate()

		return algname, result

	except Exception:
		return "Undefined", "Using is unsuccessful"

def delete(data, rights):
	try:
		algname = json.loads(data,object_hook=name)
		rightsbase = pandas.read_csv("algorithms/rights", index_col=0)
		if (rights != rightsbase.get_value(rightsbase[rightsbase.name == algname].index[0],"rights")) and (rights != "admin"):
			return algname, "Do not enough rights to delete"

		rightsbase = pandas.read_csv("algorithms/rights", index_col=0)
		rightsbase = rightsbase[rightsbase.name != algname]
		rightsbase.to_csv("algorithms/rights")

		try:
			os.remove('algorithms/'+algname)
		except Exception:
			return algname, "Algorithm does not exists"

		return algname, "Algorithm was successfully deleted"
	except Exception:
		return "Undefined", "Deleting is unsuccessful"

def show(data, rights):
	try:
		algname = json.loads(data,object_hook=name)
		rightsbase = pandas.read_csv("algorithms/rights", index_col=0)
		if (rights != rightsbase.get_value(rightsbase[rightsbase.name == algname].index[0],"rights")) and (rights != "admin"):
			return algname, "Do not enough rights to see"

		algfile = open('./algorithms/' + algname, 'r')
		alg = algfile.read()
		algfile.close()

		return algname, alg
	except Exception:
		return "Undefined", "Unable to show"




#Функции для форматирования
def ident(comm):
	return comm['ident']

def password(comm):
	return comm['password']

def command(comm):
	return comm['command']

def name(comm):
	return comm['name']

def algorithm(comm):
	return comm['algorithm']

def args(comm):
	return comm['args']

def TOTP(comm):
	return comm['TOTP']